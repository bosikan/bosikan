//
//  ServiceCellVC.swift
//  Bosikan
//
//  Created by Christian Teo on 17/09/2016.
//  Copyright © 2016 Kale. All rights reserved.
//

import UIKit

class ServiceCellVC: UITableViewCell {

    @IBOutlet weak var ServiceImage: UIImageView!
    

    @IBOutlet weak var NameOfServiceLabel: UILabel!
    
    
    @IBOutlet weak var DescriptionLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
