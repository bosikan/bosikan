//
//  ViewController.swift
//  Bosikan
//
//  Created by Kale on 13/9/16.
//  Copyright © 2016 Kale. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth

class LoginViewController: UIViewController {
    
    
    @IBOutlet weak var errorLabel: UILabel!
    @IBOutlet weak var usernameTextField: UITextField!
    
    @IBOutlet weak var userNameContainerField: UIView!
    
    @IBOutlet weak var passwordTextField: UITextField!

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        userNameContainerField.layer.cornerRadius = userNameContainerField.frame.size.height/2
        
        usernameTextField.attributedPlaceholder = NSAttributedString(string: "enter username", attributes: [NSForegroundColorAttributeName : UIColor.whiteColor()])
        
        errorLabel.hidden = true
        
        FIRAuth.auth()?.addAuthStateDidChangeListener({ (auth, user) in
            if user != nil {
                
                self.performSegueWithIdentifier("goToMessageScreen", sender: nil)
            }
        })
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func loginAction() {
        
        FIRAuth.auth()?.signInWithEmail(self.usernameTextField.text!, password: self.passwordTextField.text!, completion: { (user, error) in
            
            if (error == nil)
                
            {
                //signed in
            self.performSegueWithIdentifier("goToMessageScreen", sender: nil)
                
            }
                
            else {
                self.errorLabel.text = "Did not work"
                self.errorLabel.hidden = false
                print(error?.description)
                
            }
            
            
            
        })
        
    }



}

