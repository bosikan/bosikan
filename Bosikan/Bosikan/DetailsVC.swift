//
//  DetailsVC.swift
//  Bosikan
//
//  Created by Christian Teo on 17/09/2016.
//  Copyright © 2016 Kale. All rights reserved.
//

import UIKit
import KFSwiftImageLoader

class DetailsVC: UIViewController {
    
    @IBOutlet weak var massageImage: UIImageView!
    
    @IBOutlet weak var serviceName: UILabel!

    @IBOutlet weak var descriptionDetail: UILabel!
    
    @IBOutlet weak var launchMessage: UIButton!
    
    @IBAction func goBack(sender: AnyObject) {
        
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    
    var aService:NSMutableDictionary?
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

    serviceName.text = aService!["serviceName"] as! String
    descriptionDetail.text = aService!["description"] as! String
    massageImage.loadImageFromURLString(aService!["imageURL"] as! String)
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
