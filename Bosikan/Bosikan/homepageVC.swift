//
//  homepageVC.swift
//  Bosikan
//
//  Created by Philip Kor on 17/9/16.
//  Copyright © 2016 Kale. All rights reserved.
//

import UIKit
import Firebase
import MapKit
import KFSwiftImageLoader

class homepageVC: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var mapview: MKMapView!
    @IBOutlet weak var addservice: UIButton!
    @IBOutlet weak var menu: UIButton!
    @IBOutlet weak var searchservice: UIButton!
    @IBOutlet weak var servicesTableview: UITableView!

    var selectedService: NSMutableDictionary?
    
    @IBAction func addservice(sender: AnyObject) {
    }
    
    @IBAction func menu(sender: AnyObject) {
    }
    
    @IBAction func searchservice(sender: AnyObject) {
    }
    
    var arrayOfServices:[NSMutableDictionary] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let firebaseDB = FIRDatabase.database().reference()
        
        let serviceRef = firebaseDB.child("service")
        
        serviceRef.observeEventType(.ChildAdded, withBlock: { (snapshot) in
            
            let service = snapshot.value as! NSMutableDictionary
            service["id"] = snapshot.key
            self.arrayOfServices.append(service)
            
        })
        
        serviceRef.observeEventType(.Value, withBlock: { (snapshot) in
            
            self.servicesTableview.reloadData()
            
            if self.arrayOfServices.count > 0 {
                
                let latestPath = NSIndexPath(forRow: self.arrayOfServices.count-1, inSection: 0)
                
                self.servicesTableview.scrollToRowAtIndexPath(latestPath, atScrollPosition: .Bottom, animated: true)
            }
            
        })
        
//        let service1 = NSMutableDictionary()
//        service1["serviceName"] = "Massage"
//        service1["description"] = "Family friendly massage places"
//        service1["host"] = "Host name"
//        service1["imageURL"] = "https://redtricom.files.wordpress.com/2013/03/spotlighton3_spa1.jpg"
//        service1["latitude"] = 1.279576
//        service1["longitude"] = 103.843198
//        
//        arrayOfServices.append(service1)
//        
//        let service2 = NSMutableDictionary()
//        service2["serviceName"] = "Massage"
//        service2["description"] = "Family friendly massage places"
//        service2["host"] = "Host name"
//        service2["imageURL"] = "https://i.ytimg.com/vi/NaSd2d5rwPE/hqdefault.jpg"
//        service2["latitude"] = 1.2824872
//        service2["longitude"] = 103.8494672
//        
//        arrayOfServices.append(service2)

        servicesTableview.delegate = self
        
        servicesTableview.dataSource = self
        
        servicesTableview.reloadData()
        
//        self.reloadMapAnnotations()
        

        // Do any additional setup after loading the view.
    }

    func reloadMapAnnotations() {
        
        //clear all existing pins
        mapview.removeAnnotations(mapview.annotations)
        
        for aService in arrayOfServices {
            
            let annotation = MKPointAnnotation()
            
            let latitude = aService["latitude"] as! CLLocationDegrees
            let longitude = aService["longitude"] as! CLLocationDegrees
            let coordinates = CLLocationCoordinate2DMake(latitude, longitude)
            

            annotation.coordinate = coordinates
            
        
            annotation.title = aService["serviceName"] as! String
            
            NSOperationQueue.mainQueue().addOperationWithBlock({
                
                self.mapview.addAnnotation(annotation)
            })
            
        }
    }
    

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
        
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        
        return 1
        
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return arrayOfServices.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCellWithIdentifier("cell") as! ServiceCellVC
        
        let aService = arrayOfServices[indexPath.row]
        
        cell.NameOfServiceLabel.text = aService["serviceName"] as! String
        
        cell.DescriptionLabel.text = aService["description"] as! String
        
//        cell.ServiceImage.loadImageFromURLString(aService["imageURL"] as! String)
        
        return cell

    
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        
        return 108
        
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
      
        selectedService = arrayOfServices[indexPath.row]
        
        self.performSegueWithIdentifier("goToDetails", sender: nil)
        
        
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        if segue.identifier == "goToDetails" {
            
            let detailsVC = segue.destinationViewController as! DetailsVC
            
            detailsVC.aService = selectedService
            
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
