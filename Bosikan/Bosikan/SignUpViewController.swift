//
//  SignUpViewController.swift
//  Bosikan
//
//  Created by Christian Teo on 15/09/2016.
//  Copyright © 2016 Kale. All rights reserved.
//

import UIKit
import Firebase

class SignUpViewController: UIViewController {

    @IBOutlet weak var usernameTextField: UITextField!
    
    @IBOutlet weak var birthDatePicker: UIDatePicker!
    
    @IBOutlet weak var passwordTextField: UITextField!
    
    @IBOutlet weak var emailTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func signUpAction() {
        
        
        
        
        
        FIRAuth.auth()?.createUserWithEmail(emailTextField.text!, password: passwordTextField.text!, completion: { (user, error) in
            
            
            
            if (error != nil) {
                
                
                
            }
                
            else {
                
                
                
                self.saveUserDataToFirebase()
                
            }
            
        })
        
    }
    
    
    
    func saveUserDataToFirebase() {
        
        
        
        let birthdate = birthDatePicker.date
        
        
        
        let dateFormatter = NSDateFormatter()
        
        dateFormatter.dateFormat = "yyyyMMdd"
        
        
        
        let dateString = dateFormatter.stringFromDate(birthdate)
        
        
        
        let username = usernameTextField.text!
        
        
        
        let currentUserId = FIRAuth.auth()!.currentUser!.uid
        
        
        
        let databaseRef = FIRDatabase.database().reference()
        
        let usersRef = databaseRef.child("users")
        
        
        
        let currentUserRef = usersRef.child(currentUserId)
        
        
        
        let birthdateRef = currentUserRef.child("birthdate")
        
        
        
        let usernameRef = currentUserRef.child("username")
        
        
        
        
        
        //saves to firebase
        
        
        
        usernameRef.setValue(username)
        
        
        
        birthdateRef.setValue(dateString, withCompletionBlock: { (error, ref) in
            
            
            
            if (error != nil) {
                
                
                
            }
            else {
                self.dismissViewControllerAnimated(true, completion: nil)
            }
            
        })
}
}
