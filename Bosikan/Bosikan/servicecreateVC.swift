//
//  servicecreateVC.swift
//  Bosikan
//
//  Created by Christian Teo on 17/09/2016.
//  Copyright © 2016 Kale. All rights reserved.
//

import UIKit
import Firebase

class servicecreateVC: UIViewController {
    
    
    
    @IBOutlet weak var serviceName: UITextField!
    
    @IBOutlet weak var descriptionTextField: UITextField!
    
    
    @IBOutlet weak var postalCode: UITextField!
    
    @IBOutlet weak var datePicker: UIDatePicker!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func back(sender: AnyObject) {
    }
    
    @IBAction func submit(sender: AnyObject) {
        
        saveService()
        
    }
    func saveService() {
        
        let firebaseDB = FIRDatabase.database().reference()
        
//        let messageRef = firebaseDB.child("message")
        
        let serviceRef = firebaseDB.child("service")
        
        let newServiceRef = serviceRef.childByAutoId()
        
        let newSvc = NSMutableDictionary()
        newSvc["serviceName"] = serviceName.text
        newSvc["description"] = descriptionTextField.text
        newSvc["postalCode"] = postalCode.text
        newSvc["datePicker"] = dateStringFrom(datePicker.date)
        
        newServiceRef.setValue(newSvc) { (error, ref) in
            
            if (error != nil) {
                
                print("error saving message")
                
            }
            else {
                
                //success
                
                self.navigationController?.popViewControllerAnimated(true)
                
            }
        }
        
        
    }

    func dateStringFrom(date: NSDate) -> String {
        
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "yyyyMMdd"
        
        return dateFormatter.stringFromDate(date)
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
